#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from toh.msg import Event

def callback(msg):
    print "here is the command", str(msg.key)
    print "here is the value", msg.value

def chronology_listener():
    rospy.init_node('chronology_listener', anonymous=True)
    rospy.Subscriber('chronology', Event, callback)
    rospy.spin()

    
if __name__ == '__main__':
    chronology_listener()
