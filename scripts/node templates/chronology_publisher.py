#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from toh.msg import Event

def chronology_publisher():        
    publisher = rospy.Publisher('chronology', Event, queue_size=10)
    rospy.init_node('chronology_publisher', anonymous=True)   
    event = Event()
    event.key = "timer_out"
    event.value = 0
    rate = rospy.Rate(10)  # 10hz
    while not rospy.is_shutdown():       
        rospy.loginfo(str(event.key) + " " + str(event.value))
        publisher.publish(event)
        rate.sleep()

if __name__ == '__main__':
    try:
        chronology_publisher()
    except rospy.ROSInterruptException:
        pass