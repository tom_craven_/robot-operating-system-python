#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from toh.msg import Command

def callback(msg):
    print "here is the command", str(msg.key)
    print "here is the value", msg.value

def history_listener():
    rospy.init_node('history_listener', anonymous=True)
    rospy.Subscriber('history', Command, callback)
    rospy.spin()


if __name__ == '__main__':
    history_listener()
