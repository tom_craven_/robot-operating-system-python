#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from toh.msg import Command

def history_publisher():
    publisher = rospy.Publisher('history', Command, queue_size=10)
    rospy.init_node('history_publisher', anonymous=True)
    command = Command()
    command.key = "Go"
    command.value = True
    rospy.loginfo(str(command.key) + " " + str(command.value))
    publisher.publish(command)


if __name__ == '__main__':
    try:
        history_publisher()
    except rospy.ROSInterruptException:
        pass
