#!/usr/bin/env python

import rospy
import time
from std_msgs.msg import String
from toh.msg import Event
from toh.msg import Command

def callback(msg):

	if (msg.key == "find_wave" and msg.value == True):
		print "Step#6 look for Steve's wave return false when found"
		publisher = rospy.Publisher('history',Command, queue_size=10)
		fw = Command()
		fw.key = "find_wave"
		fw.value = False
		time.sleep(1)
		publisher.publish(fw)

	if (msg.key == "find_steve" and msg.value == True):
		publisher = rospy.Publisher('history',Command, queue_size=10)
		fs = Command()
		fs.key = "find_steve"
		fs.value = False
		time.sleep(1)
		publisher.publish(fs)
		print "Step#2 find Steve!"

	if (msg.key == "find_move" and msg.value == True):
		publisher = rospy.Publisher('chronology', Event, queue_size=10)
		event = Event()
		event.key= "game_state"
		event.value=1112
		print "Step#10 Find a move - game state found: ", event.value
		time.sleep(1)
		publisher.publish(event)

def visionNode():
	rospy.init_node('visionNode', anonymous=True)
	rospy.Subscriber('history', Command, callback)
	rospy.spin()

if __name__ == '__main__':
	try:
		visionNode()
	except rospy.ROSInterruptException:
		pass
