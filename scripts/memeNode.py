#!/usr/bin/env python

import rospy
import time
from std_msgs.msg import String
from toh.msg import Command


def callback(msg):

    if (msg.key == "explain_invite" and msg.value==True):
        print "Step#4 invite steve to game"
        publisher = rospy.Publisher('history', Command, queue_size=10)
        command = Command()
        command.key = "explain_invite"
        command.value = False
        time.sleep(1)
        publisher.publish(command)

    if (msg.key == "explain_game" and msg.value==True):
        print "Step#8 explain the game to steve"
        publisher = rospy.Publisher('history', Command, queue_size=10)
        command = Command()
        command.key = "explain_game"
        command.value = False
        time.sleep(1)
        publisher.publish(command)


def memeNode():
    rospy.init_node('memeNode', anonymous=True)
    rospy.Subscriber('history', Command, callback)
    rospy.spin()


if __name__ == '__main__':
    memeNode()
