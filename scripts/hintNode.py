#!/usr/bin/env python
#Ben Arterton 11/5/17 16:40

import rospy
from std_msgs.msg import String
from toh.msg import Event
from toh.msg import Command
global i
i = 1

def callback1(msg):
    if (msg.key=="game_state"):
        print "game state recieved"
        print str(msg.value)

        suboptimal=1
        error=0

        ##If valid state recieved
        if (valid[i]==str(msg.value)):
            print "Game state seen = " + valid[i]
	    global i
            i + 1
            print "Next state expected = " + valid[i]
            print "valid"
        ##If not a valid state then publish error
        else:
            publisher = rospy.Publisher('history', Command, queue_size=10)
            command = Command()
            command.key="incorrect_game_state"
            command.value=True
            publisher.publish(command)
            print "published"


def callback2(msg):
    ##Timer out then make a hint
    if (msg.key=="timer_out"):
        print "The expected state is "




def hanoi(n, source, helper, target):
    if n > 0:
        # Move tower of size n - 1 to helper:
        hanoi(n - 1, source, target, helper)
        # Move disk from source peg to target peg
        if source[0]:
            # Pop the disk off the current peg
            disk = source[0].pop()

            # Add it to the target peg
            target[0].append(disk)
            hanoiUser()

        #var = raw_input("Next stage...")
        hanoi(n - 1, helper, source, target)

##Function to create game state strings from the toh algo
##These will be compared with the published messages from the vision node
def hanoiUser():
    for disk in source[0]:
        if disk == 4:
            four = '1'
        if disk == 3:
            three = '1'
        if disk == 2:
            two = '1'
        if disk == 1:
            one = '1'
    for disk in target[0]:
        if disk == 4:
            four = '3'
        if disk == 3:
            three = '3'
        if disk == 2:
            two = '3'
        if disk == 1:
            one = '3'
    for disk in helper[0]:
        if disk == 4:
            four = '2'
        if disk == 3:
            three = '2'
        if disk == 2:
            two = '2'
        if disk == 1:
            one = '2'
    value = one+two+three+four
    valid.append(value)

source = ([4,3,2,1], "LEFT")
target = ([], "RIGHT")
helper = ([], "MIDDLE")

valid = ['1111']

hanoi(len(source[0]),source,helper,target)

def hint():
    rospy.init_node('hint', anonymous=True)
    rospy.Subscriber('chronology', Event, callback1)
    rospy.Subscriber('history', Command, callback2)
    try:
        hint()

    except rospy.ROSInterruptException:
        pass
