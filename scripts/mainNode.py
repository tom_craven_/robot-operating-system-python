#!/usr/bin/env python

import rospy
import time
from toh.msg import Event
from toh.msg import Command
from std_msgs.msg import String

def callback(msg):

    if (msg.key=="Go"):
        publisher = rospy.Publisher('history',Command, queue_size=10)
        go = Command()
        go.key = "find_steve"
        go.value = True
        print "Step#1 Go!"
        print "published command", str(go.key), str(go.value)
        time.sleep(1)
        publisher.publish(go)

    if (msg.key == "find_steve" and msg.value != True):
        publisher = rospy.Publisher('history',Command, queue_size=10)
        fs = Command()
        fs.key = "explain_invite"
        fs.value = True
        print "Step#3 found steve now invite him"
        time.sleep(1)
        publisher.publish(fs)

    if (msg.key == "find_wave" and msg.value != True):
        publisher = rospy.Publisher('history',Command, queue_size=10)
        fs = Command()
        fs.key = "explain_game"
        fs.value = True
        print "Step#7 Steve accepted invite now explain the game"
        time.sleep(1)
        publisher.publish(fs)

    if (msg.key == "explain_game" and msg.value != True):
        publisher = rospy.Publisher('history',Command, queue_size=10)
        eg = Command()
        eg.key = "find_move"
        eg.value = True
        print "Step#9 Game explained tell vision to look for a move"
        time.sleep(1)
        publisher.publish(eg)

    if (msg.key == "explain_invite" and msg.key != True):
        print "Step#5 tell vision to look for a wave"
        publisher = rospy.Publisher('history',Command, queue_size=10)
        fw = Command()
        fw.key = "find_wave"
        fw.value = True
        time.sleep(1)
        publisher.publish(fw)

def mainNode():
    rospy.init_node('mainNode', anonymous=True)
    rospy.Subscriber('history', Command, callback)
    rospy.spin()

if __name__ == '__main__':
    try:
        mainNode()
    except rospy.ROSInterruptException:
        pass
