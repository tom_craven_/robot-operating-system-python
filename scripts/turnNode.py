#!/usr/bin/env python
import time
import rospy
from std_msgs.msg import String
from toh.msg import Event
from toh.msg import Command

global cutOff
cutOff=0
global numTurns
numTurns=1

def turnTimer(t):
	while t > 0:
			time.sleep(1)
			t -=1
	if cutOff != 1:
		publisher = rospy.Publisher('history', Command, queue_size=10)
		command=Command()
		command.key = "timer_out"
		command.value = True
		publisher.publish(command)
		print "time out!"
	return 0

def callback2(msg):
	if (msg.key=="game_state") :
		global cutOff
		cutOff = 1
		publisher = rospy.Publisher('history', Command, queue_size=10)
		command=Command()
		command.key = "countdown stopped! move found"
		command.value = True

def callback(msg):
	if (msg.key=="find_move" and msg.value== True) :
		print "heard command", str(msg.key), str(msg.value)
		t=turnTimer(10)
		global numTurns
		numTurns +=1

def turnNode():
	rospy.init_node('turnNode', anonymous=True)
	rospy.Subscriber('history', Command, callback)
	rospy.Subscriber('Chronology', Event, callback2)
	rospy.spin()


if __name__ == '__main__':
	turnNode()
